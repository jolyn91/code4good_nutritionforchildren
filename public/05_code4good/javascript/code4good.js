(function(){
    var STARTX = 150;
    var svg = d3.select('#svg');
    var maskRect = d3.select('#mask-rect');
    var foodVolunteer = d3.select('#img-4')
        .style('opacity', 1);
    var foodCommunity = d3.select('#img-5')
        .style('opacity', 0);
    var foodParent = d3.select('#img-6')
    .style('opacity', 0);
    var foodChild = d3.select('#img-7')
        .style('opacity', 0);

    var arrow1 = d3.select('#img-8')
        .style('opacity', 0);
    var arrow2 = d3.select('#img-9')
        .style('opacity', 0);
    var arrow3 = d3.select('#img-10')
        .style('opacity', 0);

    maskRect.attr('width', STARTX);

    var bar = svg.append('rect')
        .classed('bar', true)
        .attr('x', STARTX)
        .attr('y', 0)
        .attr('width', 20)
        .attr('height', 900)
        .attr('fill', 'gray');

        bar.call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended));

    // var t = d3.transition()
    //     .duration(5000)
    //     .ease(d3.easeLinear);

    // selection.selectAll('element')
    //     .data( json.data, function(d){ return d.key } )
    //     .transition(t)
    //     .style('opacity', 0.5)

    function opacity(value, low1, high1, low2, high2) {
        return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
    }

    function dragstarted(d) {

    }

    function dragged(d) {

        d3.select(this).attr('x', d3.event.x);
        maskRect.attr('width', d3.event.x);

        foodVolunteer.style('opacity', opacity(d3.event.x, 0, 250, 0, 1));
        foodCommunity.style('opacity', opacity(d3.event.x, 250, 590, 0, 1));
        foodParent.style('opacity', opacity(d3.event.x, 590, 950, 0, 1));
        foodChild.style('opacity', opacity(d3.event.x, 950, 1400, 0, 1));

        var arrow1 = d3.select('#img-8')
            .style('opacity', opacity(d3.event.x, 250, 590, 0, 1, false));
        var arrow2 = d3.select('#img-9')
            .style('opacity', opacity(d3.event.x, 590, 950, 0, 1, false));
        var arrow3 = d3.select('#img-10')
            .style('opacity', opacity(d3.event.x, 950, 1400, 0, 1, false));
    }

    function dragended(d) {

    }
})();